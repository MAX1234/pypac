class Move:
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"Move({self.x}, {self.y})"

    def __repr__(self):
        return str(self)

    def __add__(self, other):
        if isinstance(other, Move):
            return Move(self.x + other.x, self.y + other.y)
        else:
            return Move(self.x + other, self.y + other)

    def __mul__(self, other):
        if isinstance(other, Move):
            return Move(self.x * other.x, self.y * other.y)
        else:
            return Move(self.x * other, self.y * other)

    def __eq__(self, other):
        return isinstance(other, Move) and float(other.x) == float(self.x) and float(other.y) == float(self.y)

    def as_list(self):
        return [self.x, self.y]
