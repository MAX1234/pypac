import random
from typing import Callable

from pypac import entity
from pypac import pac
from pypac.misc import Move
import pygame

GHOSTSIZE = 20
ATTACK = 0
SCATTER = 1


def closest_to(coords, my, check, last_move, mult=1, give_map=False, inv=False):
    tox = coords[0]
    toy = coords[1]
    myx = my[0]
    myy = my[1]

    lowest = float('inf') if not inv else 0
    lowest_mov = Move(1, 0) * mult
    poss_moves = [(0, mult), (0, -mult), (mult, 0), (-mult, 0)]
    maps = []
    if last_move != (0, 0) and last_move != Move(0, 0):
        try:
            poss_moves.remove((last_move[0] * -1, last_move[1] * -1))
        except ValueError as e:
            pass
            # print("Invalid: %s" % e)

    for i in poss_moves:
        if not check(myx + i[0], myy + i[1]):
            poss_moves.remove(i)

    for i, j in poss_moves:
        amt = (toy - myy - j) ** 2 + (tox - myx - i) ** 2
        if amt < lowest or (inv and amt > lowest):
            lowest = amt
            lowest_mov = Move(i, j)
            maps.append(amt)
    if give_map:
        d = dict([(x, y) for x in poss_moves for y in maps])
        d['chosen'] = lowest_mov
        return d
    return lowest_mov


class Ghost(entity.Entity):
    last_move = Move(0, 0)
    colour = tuple()
    image_path = 'assets/'
    image_name = ''
    speed = .8
    frightened = False

    corner = (0, 0)

    def __init__(self, surface, pac: pac.Pac, check_valid: Callable[[int, int], bool], inc_size, percep=False):
        self.surface = surface
        self.pac = pac
        self.check_valid = check_valid
        self.activated = False
        self.inc_size = inc_size
        self.image_name = str(self.__class__.__name__) + '.png'
        self.image = pygame.transform.scale(pygame.image.load(self.image_path + self.image_name), (GHOSTSIZE, GHOSTSIZE))
        self.scared_img = pygame.transform.scale(pygame.image.load(self.image_path + 'scared.png'),
                                                 (GHOSTSIZE, GHOSTSIZE))
        self.percep = percep
        self.map = []
        self.mode = SCATTER
        self.corner = (self.corner[0] * inc_size, self.corner[1] * inc_size)

    def get_move(self) -> Move:
        return NotImplemented

    def perceive(self):
        return NotImplemented

    def get_random_move(self):
        return random.choice([
            Move(0, 1),
            Move(1, 0),
            Move(0, -1),
            Move(-1, 0)
        ])

    def activate(self):
        self.activated = True
        self.x = 14 * self.inc_size
        self.y = 11 * self.inc_size

    def scatter(self):
        self.mode = SCATTER

    def attack(self):
        self.mode = ATTACK

    def die(self):
        print(f"Pac killed {self.__class__.__name__}")
        self.x = 12 * self.inc_size
        self.y = 14 * self.inc_size

    def move(self, *args):
        if not self.activated:
            return

        if self.mode == ATTACK and not self.frightened:
            move = self.get_move() * self.speed
        elif not self.frightened:
            move = closest_to(self.corner, (self.x, self.y), self.check_valid, self.last_move)
        else:
            move = closest_to((self.pac.x, self.pac.y), (self.x, self.y), self.check_valid, self.last_move, 1, inv=True)

        while (not self.check_valid(self.x + move.x, self.y + move.y)) or move == self.last_move:  # TODO: prevent dup mov
            move = self.get_random_move() * self.speed

        self.x += move.x
        self.y += move.y

        if int(self.x) == int(self.pac.x) and int(self.y) == int(self.pac.y):
            # Pac lost
            self.pac.die(self)

        self.last_move = tuple((move * (1 / self.speed)).as_list())  # * (1 / self.speed)

    def draw(self):
        self.surface.blit(self.image if not self.frightened else self.scared_img,
                          [(self.x // self.inc_size) * self.inc_size - GHOSTSIZE / 2,
                           (self.y // self.inc_size) * self.inc_size - GHOSTSIZE / 2,
                           GHOSTSIZE, GHOSTSIZE])
        if self.percep:
            self.perceive()


class Blinky(Ghost):
    colour = (255, 0, 0)
    corner = (27, 0)

    def get_move(self, *args):
        if self.percep:
            self.map = closest_to((self.pac.x, self.pac.y), (self.x, self.y), self.check_valid,
                                  self.last_move, give_map=True)
            mov = self.map['chosen']
        else:
            mov = closest_to((self.pac.x, self.pac.y), (self.x, self.y), self.check_valid,
                             self.last_move)
        return mov

    def perceive(self):
        print(self.map)


class Inky(Ghost):
    colour = (0, 255, 255)
    corner = (30, 30)  # Bottom Right

    def __init__(self, surface, pac: pac.Pac, check_valid: Callable[[int, int], bool], inc_size, blinky: Blinky):
        super().__init__(surface, pac, check_valid, inc_size)
        self.b = blinky

    def get_move(self, *args):
        mov = closest_to((2 * self.pac.x - self.b.x, 2 * self.pac.y - self.b.y), (self.x, self.y), self.check_valid,
                         self.last_move)
        return mov


class Pinky(Ghost):
    colour = (255, 184, 255)
    corner = (0, 0)

    def get_move(self, *args):
        mov = closest_to((Move(self.pac.x, self.pac.y) + (self.pac.dir * 4)).as_list(), (self.x, self.y), self.check_valid,
                         self.last_move)
        return mov


class Clyde(Ghost):
    colour = (255, 184, 82)
    corner = (0, 30)

    def get_move(self, *args):
        if (8 * self.inc_size) ** 2 < (self.x - self.pac.x) ** 2 + (self.y - self.pac.y) ** 2:
            mov = closest_to((self.pac.x, self.pac.y), (self.x, self.y),
                             self.check_valid, self.last_move)
            return mov

        else:
            mov = closest_to((0, 30 * self.inc_size), (self.x, self.y),
                             self.check_valid, self.last_move)
            return mov
