from typing import Callable

from pypac import entity
from pypac.misc import *
import pygame
import pygame.key

PACSIZE = 20


class Pac(entity.Entity):
    img_name = "Pacboi.png"
    colour = (253, 255, 0)
    dir = Move(0, 0)

    def __init__(self, s, check_move: Callable[[int, int], bool], normalize_coords: Callable[[int, int], Move], inc_size):
        self.surface = s
        self.check = check_move
        self.norm = normalize_coords
        self.inc_size = inc_size
        self.image = pygame.transform.scale(pygame.image.load('assets/' + self.img_name), (PACSIZE, PACSIZE))
        self.rotated_img = self.image
        self.dead = False

    def draw(self):
        locX = self.x
        locY = self.y
        self.surface.blit(self.rotated_img, [(locX // self.inc_size) * self.inc_size - PACSIZE / 2,
                                             (locY // self.inc_size) * self.inc_size - PACSIZE / 2,
                                              PACSIZE, PACSIZE])
        # pygame.draw.rect(self.surface, self.colour, [(locX // self.inc_size) * self.inc_size - PACSIZE / 2,
        #                                              (locY // self.inc_size) * self.inc_size - PACSIZE / 2,
        #                                              PACSIZE, PACSIZE])

    def get_move(self, *args):
        return self.dir

    def die(self, killer):
        if not self.dead:
            print(f"Pacman was killed by {killer.__class__.__name__}")
            self.dead = True  # TODO: Die

    def move(self, *_):
        # Update
        if pygame.key.get_pressed()[pygame.K_w]:
            self.dir = Move(0, -1)
            self.rotated_img = pygame.transform.rotate(self.image, 90)
        elif pygame.key.get_pressed()[pygame.K_a]:
            self.dir = Move(-1, 0)
            self.rotated_img = pygame.transform.rotate(self.image, 180)
        elif pygame.key.get_pressed()[pygame.K_s]:
            self.rotated_img = pygame.transform.rotate(self.image, 270)
            self.dir = Move(0, 1)
        elif pygame.key.get_pressed()[pygame.K_d]:
            self.dir = Move(1, 0)
            self.rotated_img = pygame.transform.rotate(self.image, 0)

        n = self.norm(self.x, self.y)
        if self.check(self.dir.x + n.x, self.dir.y + n.y):
            self.x += self.dir.x
            self.y += self.dir.y
        # else:
        #     print("Invalid move x = %i y = %i dx = %i dy = %i" % (n.x, n.y, self.dir.x, self.dir.y))
