from pypac import ghosts
from pypac import pac
import pygame
import sys
import time

from pypac.misc import Move

ENTITIES = []

COLOURS = {
    0: (21, 63, 132),
    1: (0, 0, 0),
    2: (21, 63, 132),
    3: (0,0,0),
    4: (21, 63, 132)
}

PELLET_COLOURS = {
    2: (253, 255, 0),
    4: (253, 255, 0)
}

TILES = [
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [1,2,2,2,2,2,2,2,2,2,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,2,2,1],
    [1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1],
    [1,4,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,4,1],
    [1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1],
    [1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1],
    [1,2,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,2,1],
    [1,2,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,2,1],
    [1,2,2,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,2,2,1],
    [1,1,1,1,1,1,2,1,1,1,1,1,0,1,1,0,1,1,1,1,1,2,1,1,1,1,1,1],
    [1,1,1,1,1,1,2,1,1,1,1,1,0,1,1,0,1,1,1,1,1,2,1,1,1,1,1,1],
    [1,1,1,1,1,1,2,1,1,0,0,0,0,0,0,0,0,0,0,1,1,2,1,1,1,1,1,1],
    [1,1,1,1,1,1,2,1,1,0,1,1,1,1,1,1,1,1,0,1,1,2,1,1,1,1,1,1],
    [1,1,1,1,1,1,2,1,1,0,1,0,0,0,0,0,0,1,0,1,1,2,1,1,1,1,1,1],
    [0,0,0,0,0,0,2,0,0,0,1,0,0,0,0,0,0,1,0,0,0,2,0,0,0,0,0,0],
    [1,1,1,1,1,1,2,1,1,0,1,0,0,0,0,0,0,1,0,1,1,2,1,1,1,1,1,1],
    [1,1,1,1,1,1,2,1,1,0,1,1,1,1,1,1,1,1,0,1,1,2,1,1,1,1,1,1],
    [1,1,1,1,1,1,2,1,1,0,0,0,0,0,0,0,0,0,0,1,1,2,1,1,1,1,1,1],
    [1,1,1,1,1,1,2,1,1,0,1,1,1,1,1,1,1,1,0,1,1,2,1,1,1,1,1,1],
    [1,1,1,1,1,1,2,1,1,0,1,1,1,1,1,1,1,1,0,1,1,2,1,1,1,1,1,1],
    [1,2,2,2,2,2,2,2,2,2,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,2,2,1],
    [1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1],
    [1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1],
    [1,4,2,2,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,2,2,4,1],
    [1,1,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2,1,1,1],
    [1,1,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2,1,1,1],
    [1,2,2,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,2,2,1],
    [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1],
    [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1],
    [1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
]

DOTS_EATEN = 0

DIM_X = len(TILES[0])
INC_X = 20
DIM_Y = len(TILES)
INC_Y = INC_X

PELLETS = [[w, h] for w in range(DIM_X) for h in range(DIM_Y) if TILES[h][w] in [2, 4]]
SUPER_PELLETS = [[w, h] for w in range(DIM_X) for h in range(DIM_Y) if TILES[h][w] == 4]
PAC_START = (14 * INC_X, 17 * INC_Y)

W = INC_X * DIM_X
H = INC_Y * DIM_Y

W -= INC_X // 2
H -= INC_Y // 2

W = int(W)
H = int(H)

PINKY_LIMIT = 0
INKY_LIMIT = 30
CLYDE_LIMIT = 90

start = time.time()
ATTACK = 0
SCATTER = 1
GMODE = SCATTER


def all_chase():
    for i in ENTITIES:
        if hasattr(i, 'attack'):
            i.attack()


def all_frightened(f=True):
    for i in ENTITIES:
        if hasattr(i, 'frightened'):
            i.frightened = f


def all_scatter():
    for i in ENTITIES:
        if hasattr(i, 'scatter'):
            i.scatter()


def check_move(x, y) -> bool:
    y = int(y)
    x = int(x)

    # print(cond1, cond2, cond3)
    return 0 <= y < DIM_Y and 0 <= x < DIM_X and TILES[y][x] != 1


def check_coords(x, y) -> bool:
    y = int(y // INC_Y)
    x = int(x // INC_X)

    # print(cond1, cond2, cond3)
    return 0 <= y < DIM_Y and 0 <= x < DIM_X and TILES[y][x] != 1


def normalize_coords(x, y) -> Move:
    return Move(x // INC_X, y // INC_Y)


def normalize_coords_list(x, y):
    return [int(x // INC_X), int(y // INC_Y)]


def init(w=W, h=H):
    global ENTITIES, s, Blinky, Pinky, Inky, Clyde

    pygame.init()
    s = pygame.display.set_mode((w, h))

    # Add pac, i/b/p/c
    p = pac.Pac(s, check_move, normalize_coords, INC_X)
    p.x = PAC_START[0]
    p.y = PAC_START[1]
    ENTITIES.append(p)
    Blinky = ghosts.Blinky(s, p, check_coords, INC_X)
    Blinky.x = 12 * INC_X
    Blinky.y = 14 * INC_Y
    ENTITIES.append(Blinky)
    Inky = ghosts.Inky(s, p, check_coords, INC_X, Blinky)
    Inky.x = 14 * INC_X
    Inky.y = 14 * INC_Y
    ENTITIES.append(Inky)
    Pinky = ghosts.Pinky(s, p, check_coords, INC_X)
    Pinky.x = 13 * INC_X
    Pinky.y = 14 * INC_Y
    ENTITIES.append(Pinky)
    Clyde = ghosts.Clyde(s, p, check_coords, INC_X)
    Clyde.x = 15 * INC_X
    Clyde.y = 14 * INC_Y
    ENTITIES.append(Clyde)

    Blinky.activate()
    # Inky.activate()
    # Pinky.activate()
    # Clyde.activate()
    # ENTITIES.append(ghosts.Inky(s, p, check_move))
    # ENTITIES.append(ghosts.Blinky(s, p, check_move))
    # ENTITIES.append(ghosts.Pinky(s, p, check_move))
    # ENTITIES.append(ghosts.Clyde(s, p, check_move))


def draw_tiles():
    for i in range(DIM_X):
        for j in range(DIM_Y):
            colour = COLOURS[TILES[j][i]]
            rect = ((i - 1/2) * INC_X, (j - 1/2) * INC_Y, INC_X, INC_Y)
            pygame.draw.rect(s, colour, rect)

    for i in PELLETS:
        colour = PELLET_COLOURS[2]
        pos = (int(i[0] * INC_X), int(i[1] * INC_Y))
        radius = ((i in SUPER_PELLETS) + 1) * 4
        pygame.draw.circle(s, colour, pos, radius)


__CHASE1 = False
__SCATTER1 = False
__CHASE2 = False
__SCATTER2 = False
__CHASE3 = False
__SCATTER3 = False
__PERMCHASE = False
started_fright = 0


def mainloop():
    global PELLETS, DOTS_EATEN, __CHASE1, __CHASE2, __CHASE3, __SCATTER1, __SCATTER2, __SCATTER3, __PERMCHASE, \
        started_fright

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    if ENTITIES[0].dead:
        return

    draw_tiles()

    for i in ENTITIES:
        i.move()
        i.draw()

        if i.__class__ == pac.Pac:
            try:
                coords = normalize_coords_list(i.x, i.y)
                PELLETS.remove(coords)
                DOTS_EATEN += 1  # TODO: Power Pellets
                if coords in SUPER_PELLETS:
                    all_frightened()
                    started_fright = time.time()

                if not Pinky.activated and DOTS_EATEN > PINKY_LIMIT:
                   Pinky.activate()
                if not Inky.activated and DOTS_EATEN > INKY_LIMIT:
                    Inky.activate()
                if not Clyde.activated and DOTS_EATEN > CLYDE_LIMIT:
                    Clyde.activate()
            except ValueError:
                pass

    if time.time() - start >= 7 and not __CHASE1:
        __CHASE1 = True
        print("Chase 1")
        all_chase()
    elif time.time() - start >= 27 and not __SCATTER1:
        __SCATTER1 = True
        print("Scatter 1")
        all_scatter()
    elif time.time() - start >= 34 and not __CHASE2:
        __CHASE2 = True
        print("Chase 2")
        all_chase()
    elif time.time() - start >= 54 and not __SCATTER2:
        __SCATTER2 = True
        print("Scatter 2")
        all_scatter()
    elif time.time() - start >= 59 and not __CHASE3:
        __CHASE3 = True
        print("Chase 3")
        all_chase()
    elif time.time() - start >= 74 and not __SCATTER3:
        __SCATTER3 = True
        print("Scatter 3")
        all_scatter()
    elif time.time() - start >= 79 and not __PERMCHASE:
        __PERMCHASE = True
        print("Permanent Chase")
        all_chase()

    if time.time() - started_fright > 10:
        all_frightened(False)

    pygame.display.flip()
